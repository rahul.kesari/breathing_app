package com.mukundan.breathe_mindfulbreathingapp.data.remote

import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel

/**
 * Created by rahul
 */

internal interface RemoteSource {
    suspend fun requestNews(): Resource<NewsModel>
}
