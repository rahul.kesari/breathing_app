package com.mukundan.breathe_mindfulbreathingapp.data

import com.mukundan.breathe_mindfulbreathingapp.data.local.LocalRepository
import com.mukundan.breathe_mindfulbreathingapp.data.remote.RemoteRepository
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import javax.inject.Inject


/**
 * Created by rahul
 */

class DataRepository @Inject
constructor(private val remoteRepository: RemoteRepository, private val localRepository: LocalRepository) : DataSource {

    override suspend fun requestNews(): Resource<NewsModel> {
        return remoteRepository.requestNews()
    }
}
