package com.mukundan.breathe_mindfulbreathingapp.data.remote.dto


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HomeDataModel(
    @Json(name = "sleep")
    var sleepItems: List<SleepItem> = listOf(),
    @Json(name = "meditation")
    var meditationItems: List<MeditationItem> = listOf(),
    @Json(name = "relaxe")
    var relaxeItem: List<RelaxeItem> = listOf()
)