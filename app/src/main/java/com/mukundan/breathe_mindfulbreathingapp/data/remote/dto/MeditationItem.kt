package com.mukundan.breathe_mindfulbreathingapp.data.remote.dto


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MeditationItem(
        @Json(name = "id")
        val id: Int = 0
)