package com.mukundan.breathe_mindfulbreathingapp.data

import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel

/**
 * Created by rahul
 */

interface DataSource {
    suspend fun requestNews(): Resource<NewsModel>
}
