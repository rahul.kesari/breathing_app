/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mukundan.breathe_mindfulbreathingapp.di

import androidx.lifecycle.ViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.accountsettings.AccountSettingsViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.completedsession.CompletedSessionViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.detail.DetailViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.freepremium.FreePremiumSubscriptionViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.MainViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.homefragment.HomeViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.MeditationViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.meditationchildfragments.MeditationChildViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.profile.ProfileViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe.RelaxeViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe.relaxechildfragments.RelaxeChildViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.SleepViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.sleepchildfragments.SleepChildViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.livesession.LiveSessionViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.login.LoginViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.managesubscription.ManageSubscriptionViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies.MelodiesViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies.fragments.MelodiesFragmentViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.moodchart.MoodChartViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.musicplay.MusicPlayViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.news.NewsListViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.reminders.RemindersViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.settings.SettingsViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.signup.SignupViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.splash.SplashViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.startbreathing.StartbreathingViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(NewsListViewModel::class)
    abstract fun bindUserViewModel(viewModel: NewsListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: DetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindMainActivityViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeFragmentViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SleepViewModel::class)
    internal abstract fun bindSleepFragmentViewModel(viewModel: SleepViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MeditationViewModel::class)
    internal abstract fun bindMeditationFragmentViewModel(viewModel: MeditationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RelaxeViewModel::class)
    internal abstract fun bindRelaxeFragmentViewModel(viewModel: RelaxeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    internal abstract fun bindProfileFragmentViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignupViewModel::class)
    internal abstract fun bindSignupViewModel(viewModel: SignupViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SleepChildViewModel::class)
    internal abstract fun bindSleepChildViewModel(viewModel: SleepChildViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MeditationChildViewModel::class)
    internal abstract fun bindMeditationChildViewModel(viewModel: MeditationChildViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RelaxeChildViewModel::class)
    internal abstract fun bindRelaxeChildViewModel(viewModel: RelaxeChildViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StartbreathingViewModel::class)
    internal abstract fun bindStartbreathingViewModel(viewModel: StartbreathingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CompletedSessionViewModel::class)
    internal abstract fun bindCompletedSessionViewModel(viewModel: CompletedSessionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LiveSessionViewModel::class)
    internal abstract fun bindLiveSessionViewModel(viewModel: LiveSessionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MusicPlayViewModel::class)
    internal abstract fun bindMusicPlayViewModel(viewModel: MusicPlayViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun bindDetailModel(viewModel: DetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MelodiesViewModel::class)
    internal abstract fun bindMelodiesModel(viewModel: MelodiesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MelodiesFragmentViewModel::class)
    internal abstract fun bindMelodiesFragmentModel(viewModel: MelodiesFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    internal abstract fun bindSettingsViewModel(viewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MoodChartViewModel::class)
    internal abstract fun bindMoodChartViewModel(viewModel: MoodChartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountSettingsViewModel::class)
    internal abstract fun bindAccountSettingsViewModel(viewModel: AccountSettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManageSubscriptionViewModel::class)
    internal abstract fun bindManageSubscriptionViewModel(viewModel: ManageSubscriptionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FreePremiumSubscriptionViewModel::class)
    internal abstract fun bindFreePremiumSubscriptionViewModel(viewModel: FreePremiumSubscriptionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RemindersViewModel::class)
    internal abstract fun bindRemindersViewModel(viewModel: RemindersViewModel): ViewModel
}
