/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mukundan.breathe_mindfulbreathingapp.di

import com.mukundan.breathe_mindfulbreathingapp.ui.component.accountsettings.AccountSettingsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.completedsession.CompletedSessionActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.detail.DetailActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.freepremium.FreePremiumSubscripitonActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.MainActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.homefragment.HomeFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.MeditationFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.meditationchildfragments.MeditationChildFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.profile.ProfileFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe.RelaxeFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe.relaxechildfragments.RelaxeChildFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.SleepFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.sleepchildfragments.SleepChildFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.livesession.LiveSessionActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.login.LoginActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.managesubscription.ManageSubscriptionActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies.MelodiesActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies.fragments.MelodiesFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.moodchart.MoodChartActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.musicplay.MusicPlayActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.news.NewsListActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.reminders.RemindersActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.settings.SettingsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.signup.SignupActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.splash.SplashActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.startbreathing.StartbreathingActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModuleBuilder {
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun contributeNewsActivity(): NewsListActivity

    @ContributesAndroidInjector
    abstract fun contributeDetailsActivity(): DetailsActivity

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeSleepFragment(): SleepFragment

    @ContributesAndroidInjector
    abstract fun contributeMeditationFragment(): MeditationFragment

    @ContributesAndroidInjector
    abstract fun contributeRelaxeFragment(): RelaxeFragment

    @ContributesAndroidInjector
    abstract fun contributeProfilleFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun contributeSignupActivity(): SignupActivity

    @ContributesAndroidInjector
    abstract fun contributeSleepChildFragment(): SleepChildFragment

    @ContributesAndroidInjector
    abstract fun contributeMeditationChildFragment(): MeditationChildFragment

    @ContributesAndroidInjector
    abstract fun contributeRelaxeChildFragment(): RelaxeChildFragment

    @ContributesAndroidInjector
    abstract fun contributeStartbreathingActivity(): StartbreathingActivity

    @ContributesAndroidInjector
    abstract fun contributeCompletedSessionActivity(): CompletedSessionActivity

    @ContributesAndroidInjector
    abstract fun contributeLiveSessionActivity(): LiveSessionActivity

    @ContributesAndroidInjector
    abstract fun contributeMusicPlayActivity(): MusicPlayActivity

    @ContributesAndroidInjector
    abstract fun contributeDetailActivity(): DetailActivity

    @ContributesAndroidInjector
    abstract fun contributeMelodiesActivity(): MelodiesActivity

    @ContributesAndroidInjector
    abstract fun contributeMelodiesFragment(): MelodiesFragment

    @ContributesAndroidInjector
    abstract fun contributeMoodChartActivity(): MoodChartActivity

    @ContributesAndroidInjector
    abstract fun contributeSettingsActivity(): SettingsActivity

    @ContributesAndroidInjector
    abstract fun contributeAccountSettingsActivity(): AccountSettingsActivity

    @ContributesAndroidInjector
    abstract fun contributeManageSubscriptionActivity(): ManageSubscriptionActivity

    @ContributesAndroidInjector
    abstract fun contributeFreePremiumSubscriptionActivity(): FreePremiumSubscripitonActivity

    @ContributesAndroidInjector
    abstract fun contributeRemindersActivity(): RemindersActivity
}
