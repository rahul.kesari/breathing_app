
package com.mukundan.breathe_mindfulbreathingapp.di

import com.mukundan.breathe_mindfulbreathingapp.data.DataRepository
import com.mukundan.breathe_mindfulbreathingapp.data.DataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

// Tells Dagger this is a Dagger module
@Module
abstract class DataModule {
    @Binds
    @Singleton
    abstract fun provideDataRepository(dataRepository: DataRepository): DataSource
}
