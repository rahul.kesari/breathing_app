package com.mukundan.breathe_mindfulbreathingapp.utils

/**
 * Created by rahul
 */

class Constants {
    companion object INSTANCE {
        const val SPLASH_DELAY = 3000
        const val BASE_URL = "https://api.nytimes.com/svc/"
        const val NEWS_ITEM_KEY = "NEWS_ITEM_KEY"
    }

    object PREFS {
        const val CURRENT_STYLE = "CURRENT_STYLE"
    }
}
