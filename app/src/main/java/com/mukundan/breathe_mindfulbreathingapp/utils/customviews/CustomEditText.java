package com.mukundan.breathe_mindfulbreathingapp.utils.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.mukundan.breathe_mindfulbreathingapp.R;


/**
 * Created by rahul on 11/4/17.
 */

public class CustomEditText extends AppCompatEditText {
    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context,attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context,attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;

        switch (asset){
            case "extrabold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Muli-ExtraBold.ttf");
                break;
            case "bold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Muli-Bold.ttf");
                break;
            case "light":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Muli-Light.ttf");
                break;
            case "medium":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Muli-Black.ttf");
                break;
            case "regular":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Muli-Regular.ttf");
                break;
            case "semibold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Muli-SemiBold.ttf");
                break;
        }
        setTypeface(tf);
        return true;
    }
}
