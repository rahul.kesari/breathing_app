import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Rect
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.mukundan.breathe_mindfulbreathingapp.App
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.ui.component.accountsettings.AccountSettingsActivity


public class UiHelper {


    companion object {
        private val TAG = UiHelper::class.java.getCanonicalName()


        fun changeIconsColorsToWhite(imageView: ImageView) {
            imageView.setColorFilter(
                Color.WHITE, PorterDuff.Mode.SRC_ATOP
            )
        }

        fun changeIconsColorsToBlack(imageView: ImageView) {
            imageView.setColorFilter(
                Color.BLACK, PorterDuff.Mode.SRC_ATOP
            )
        }

        fun changeIconsColorsToBlue(imageView: ImageView) {
            imageView.setColorFilter(
                ContextCompat.getColor(App.context, R.color.color_blue), PorterDuff.Mode.SRC_ATOP
            )
        }

        fun changeIconsColorsToRed(imageView: ImageView) {
            imageView.setColorFilter(
                ContextCompat.getColor(App.context, R.color.color_e9333c), PorterDuff.Mode.SRC_ATOP
            )
        }

    }


}
