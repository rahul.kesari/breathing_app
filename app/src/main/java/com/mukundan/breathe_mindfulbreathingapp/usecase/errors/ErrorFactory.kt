package com.mukundan.breathe_mindfulbreathingapp.usecase.errors

import com.mukundan.breathe_mindfulbreathingapp.data.error.Error

interface ErrorFactory {
    fun getError(errorCode: Int): Error
}