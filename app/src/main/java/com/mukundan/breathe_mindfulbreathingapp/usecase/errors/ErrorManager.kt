package com.mukundan.breathe_mindfulbreathingapp.usecase.errors

import com.mukundan.breathe_mindfulbreathingapp.data.error.Error
import com.mukundan.breathe_mindfulbreathingapp.data.error.mapper.ErrorMapper
import javax.inject.Inject

/**
 * Created by rahull
 */

class ErrorManager @Inject constructor(private val errorMapper: ErrorMapper) : ErrorFactory {
    override fun getError(errorCode: Int): Error {
        return Error(code = errorCode, description = errorMapper.errorsMap.getValue(errorCode))
    }

}