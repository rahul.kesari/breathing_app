package com.mukundan.breathe_mindfulbreathingapp.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.ProgressItemBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.SleepItemBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.viewholder.ProgressViewHolder
import com.mukundan.breathe_mindfulbreathingapp.ui.base.listeners.RecyclerItemListener
import com.mukundan.breathe_mindfulbreathingapp.ui.component.detail.DetailActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.sleepchildfragments.SleepChildViewModel

/**
 * Created by rahul
 */

class SleepChildAdapter(
    private val sleepChildViewModel: SleepChildViewModel,
    private val sleeps: List<NewsItem>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context

    private val onItemClickListener: RecyclerItemListener = object : RecyclerItemListener {
        override fun onItemSelected(sleepsItem: NewsItem) {
//            sleepChildViewModel.openNewsDetails(sleepsItem)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                SleepItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return SleepViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SleepViewHolder) {
            (holder as SleepViewHolder).bind(sleeps[position], onItemClickListener, mContext)
        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return sleeps.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (sleeps.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class SleepViewHolder(private val itemBinding: SleepItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(
            sleepsItem: NewsItem,
            recyclerItemListener: RecyclerItemListener,
            context: Context
        ) {
//            itemBinding.tvCaption.text = sleepsItem.abstractInfo
//            itemBinding.tvTitle.text = sleepsItem.title
//
//            if (sleepsItem.multimedia.size > 3) {
//                val url: String? = sleepsItem.multimedia[3].url
//                Picasso.get().load(url).placeholder(R.color.color_placeholder)
//                    .error(R.color.color_placeholder).into(itemBinding.ivNewsItemImage)
//            }
//            itemBinding.rlNewsItem.setOnClickListener { recyclerItemListener.onItemSelected(sleepsItem) }
            itemBinding.parentCard.setOnClickListener {
                DetailActivity.start(context = context)
            }
        }
    }


}

