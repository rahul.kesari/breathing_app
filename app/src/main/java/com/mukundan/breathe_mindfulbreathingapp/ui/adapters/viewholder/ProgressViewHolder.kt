package com.mukundan.breathe_mindfulbreathingapp.ui.adapters.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.mukundan.breathe_mindfulbreathingapp.databinding.ProgressItemBinding

class ProgressViewHolder(private val itemBinding: ProgressItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {
    fun bind() {
        itemBinding.progressBar1.setIndeterminate(true)
    }
}