package com.mukundan.breathe_mindfulbreathingapp.ui.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.MelodiesItemCardBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.ProgressItemBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.SleepItemBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.viewholder.ProgressViewHolder
import com.mukundan.breathe_mindfulbreathingapp.ui.base.listeners.RecyclerItemListener
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.meditationchildfragments.MeditationChildViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies.MelodiesActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies.fragments.MelodiesFragmentViewModel
import com.mukundan.breathe_mindfulbreathingapp.utils.ThemeHelper

/**
 * Created by rahul
 */

class MelodiesAdapter(
    private val context: Activity?,
    private val melodiesfragmentViewModel: MelodiesFragmentViewModel,
    private val meditations: List<NewsItem>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private val onItemClickListener: RecyclerItemListener = object : RecyclerItemListener {
        override fun onItemSelected(meditationsItem: NewsItem) {
//            meditationChildViewModell.openNewsDetails(meditationsItem)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                MelodiesItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return MelodiesViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MelodiesViewHolder) {
            (holder as MelodiesViewHolder).bind(meditations[position], onItemClickListener,context)

        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return meditations.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (meditations.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class MelodiesViewHolder(private val itemBinding: MelodiesItemCardBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(meditationsItem: NewsItem, recyclerItemListener: RecyclerItemListener,activity: Activity?) {
//            itemBinding.tvCaption.text = meditationsItem.abstractInfo
//            itemBinding.tvTitle.text = meditationsItem.title
//
//            if (meditationsItem.multimedia.size > 3) {
//                val url: String? = meditationsItem.multimedia[3].url
//                Picasso.get().load(url).placeholder(R.color.color_placeholder)
//                    .error(R.color.color_placeholder).into(itemBinding.ivNewsItemImage)
//            }
//            itemBinding.rlNewsItem.setOnClickListener { recyclerItemListener.onItemSelected(meditationsItem) }

            if (ThemeHelper.isDarkTheme(activity!!)) {
                itemBinding.topRl.background= ContextCompat.getDrawable(activity, R.drawable.black_glow_light)
            } else {
                itemBinding.topRl.background= ContextCompat.getDrawable(activity, R.drawable.blue_glow_light)
            }
        }
    }


}

