package com.mukundan.breathe_mindfulbreathingapp.ui.base

import androidx.lifecycle.ViewModel
import com.mukundan.breathe_mindfulbreathingapp.usecase.errors.ErrorManager


/**
 * Created by rahul
 */


abstract class BaseViewModel : ViewModel() {
    /**Inject Singlton ErrorManager
     * Use this errorManager to get the Errors
     */
    abstract val errorManager: ErrorManager

}
