package com.mukundan.breathe_mindfulbreathingapp.ui.component.startbreathing

import UiHelper
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.StartBreathingActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.completedsession.CompletedSessionActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.dialogs.BreatheControllerDialog
import com.mukundan.breathe_mindfulbreathingapp.ui.dialogs.BreatheControllerDialog.UpdateListener
import com.mukundan.breathe_mindfulbreathingapp.utils.*
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants.INSTANCE.NEWS_ITEM_KEY
import javax.inject.Inject


/**
 * Created by rahul
 */

class StartbreathingActivity : BaseActivity() {
    private lateinit var binding: StartBreathingActivityBinding

    @Inject
    lateinit var startbreathingViewModel: StartbreathingViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = StartBreathingActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        startbreathingViewModel = viewModelFactory.create(StartbreathingViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        changeIconsColors()
        initTimers()
        initViews()
        setupInsets()
//        setupWindowAnimations()
    }


    private fun initViews() {
        binding.playPauseBtn.setOnClickListener {
            CompletedSessionActivity.start(this)
        }

        binding.filterBtn.setOnClickListener {

            val breatheControllerDialog = BreatheControllerDialog();
            breatheControllerDialog.setUpDialog(
                this,
                object : BreatheControllerDialog.UpdateListener {
                    override fun onResetClicked() {

                    }
                })

            breatheControllerDialog.showDialog()
        }
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(startbreathingViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }

    private fun navigateToDetailsScreen(navigateEvent: Event<NewsItem>) {
        navigateEvent.getContentIfNotHandled()?.let {
            val nextScreenIntent = Intent(this, DetailsActivity::class.java).apply {
                putExtra(NEWS_ITEM_KEY, it)
            }
            startActivity(nextScreenIntent)
        }
    }

    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        startbreathingViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        startbreathingViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { startbreathingViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(startbreathingViewModel.newsLiveData, ::handleNewsList)
        observe(startbreathingViewModel.newsSearchFound, ::showSearchResult)
        observe(startbreathingViewModel.noSearchFound, ::noSearchResult)
        observeEvent(startbreathingViewModel.openNewsDetails, ::navigateToDetailsScreen)
        observeSnackBarMessages(startbreathingViewModel.showSnackBar)
        observeToast(startbreathingViewModel.showToast)

    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, StartbreathingActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide
    }

    private fun changeIconsColors() {

        UiHelper.changeIconsColorsToWhite(binding.currnetModeIv)
        UiHelper.changeIconsColorsToWhite(binding.backBtn)
        UiHelper.changeIconsColorsToWhite(binding.filterBtn)
        UiHelper.changeIconsColorsToWhite(binding.playPauseIv)

        if(ThemeHelper.isDarkTheme(this)){
            UiHelper.changeIconsColorsToWhite(binding.ivMoon)
            UiHelper.changeIconsColorsToWhite(binding.ivRelaxe)
            UiHelper.changeIconsColorsToWhite(binding.ivFpbe)
        }else {
            UiHelper.changeIconsColorsToBlack(binding.ivMoon)
            UiHelper.changeIconsColorsToBlack(binding.ivRelaxe)
            UiHelper.changeIconsColorsToBlack(binding.ivFpbe)
        }
    }

    private fun initTimers() {
        val data = arrayOf(
            "01 min",
            "02 mins",
            "03 mins",
            "04 mins",
            "05 mins",
            "06 mins",
            "07 mins",
            "08 mins",
            "09 mins",
            "10 mins",
            "11 mins",
            "12 mins"
        )

        binding.timePicker.minValue = 1
        binding.timePicker.maxValue = data.size
        binding.timePicker.displayedValues = data
        binding.timePicker.value = 3
    }


    private fun setupInsets() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.topLayout.systemUiVisibility = SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        } else {
//            moviesRecyclerView.updatePadding(top = toolbarHeight + baseMoviesPadding)
        }
        ViewCompat.setOnApplyWindowInsetsListener(
            binding.topLayout
        ) { v: View?, insets: WindowInsetsCompat ->
            val params = binding.topLayout.layoutParams as FrameLayout.LayoutParams
            params.setMargins(0, 0, 0, insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
    }

}
