package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.sleepchildfragments

import com.mukundan.breathe_mindfulbreathingapp.data.error.mapper.ErrorMapper
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseViewModel
import com.mukundan.breathe_mindfulbreathingapp.usecase.errors.ErrorManager
import javax.inject.Inject

/**
 * Created by rahul
 */

class SleepChildViewModel @Inject
constructor() : BaseViewModel(){
    override val errorManager: ErrorManager
        get() = ErrorManager(ErrorMapper())
}
