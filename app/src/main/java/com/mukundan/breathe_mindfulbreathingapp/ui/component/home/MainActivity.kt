package com.mukundan.breathe_mindfulbreathingapp.ui.component.home

import android.content.Context
import android.content.Intent
import android.view.MenuItem
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.databinding.MainActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.pageradapter.HomePagerAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.homefragment.HomeFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.MeditationFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.profile.ProfileFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe.RelaxeFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.SleepFragment
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants
import com.mukundan.breathe_mindfulbreathingapp.utils.L
import com.mukundan.breathe_mindfulbreathingapp.utils.Prefs
import com.mukundan.breathe_mindfulbreathingapp.utils.ThemeHelper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject


/**
 * Created by rahul
 */

class MainActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var mainViewModel: MainViewModel

    private lateinit var binding: MainActivityBinding

    private var TAG: String = MainActivity.javaClass.simpleName;


    override fun initViewBinding() {

        binding = MainActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initViews()
    }


    override fun initializeViewModel() {
        mainViewModel = viewModelFactory.create(MainViewModel::class.java)
    }

    override fun observeViewModel() {

    }


    private fun initViews() {
        val adapter = HomePagerAdapter(supportFragmentManager)
        adapter.addFragment(HomeFragment.newInstance(), "")
        adapter.addFragment(SleepFragment.newInstance(), "")
        adapter.addFragment(MeditationFragment.newInstance(), "")
        adapter.addFragment(RelaxeFragment.newInstance(), "")
        adapter.addFragment(ProfileFragment.newInstance(), "")

        binding.viewPager.disableScroll(true)
        binding.viewPager.offscreenPageLimit = 5
        binding.viewPager.adapter = adapter


        binding.navigation.setOnNavigationItemSelectedListener(object :
            BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
                val title: String = menuItem.title.toString()
                L.d(TAG, "title >>>>" + title)
                if (title.equals(getString(R.string.title_home))) {
                    navigateToFragment(0)
                    return true
                } else if (title.equals(getString(R.string.title_sleep))) {
                    navigateToFragment(1)
                    return true
                } else if (title.equals(getString(R.string.title_meditation))) {
                    navigateToFragment(2)
                    return true
                } else if (title.equals(getString(R.string.title_relax))) {
                    navigateToFragment(3)
                    return true
                } else if (title.equals(getString(R.string.title_profile))) {
                    navigateToFragment(4)
                    return true
                }
                return true
            }
        })

    }

    private fun navigateToFragment(position: Int) {
        binding.viewPager.setCurrentItem(position, true)
    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(currentTheme: String?) {
        recreate()
    }

    override fun onDestroy() {

        // mPresenter.onDetach();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
        super.onDestroy()

    }
}