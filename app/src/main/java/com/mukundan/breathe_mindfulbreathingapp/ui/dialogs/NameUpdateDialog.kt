package com.mukundan.breathe_mindfulbreathingapp.ui.dialogs;

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.databinding.NameUpdateDialogBinding



/**
 * Created by rahul on 1/15/2018.
 */


public class NameUpdateDialog(context: Context?) : Dialog(context!!) {

    private lateinit var binding: NameUpdateDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        //window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding = NameUpdateDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViews()
    }


    interface OkListener {
        fun onClicked(matchid: String?)
    }

    private fun initViews() {
     
    }

}