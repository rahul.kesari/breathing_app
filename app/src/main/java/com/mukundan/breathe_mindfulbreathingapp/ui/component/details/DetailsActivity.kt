package com.mukundan.breathe_mindfulbreathingapp.ui.component.details

import android.os.Bundle
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.DetailsLayoutBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants
import com.mukundan.breathe_mindfulbreathingapp.utils.observe
import javax.inject.Inject

/**
 * Created by rahul
 */

class DetailsActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: DetailsViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: DetailsLayoutBinding


    override fun initViewBinding() {
        binding = DetailsLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.newsItem.value = intent.getParcelableExtra(Constants.NEWS_ITEM_KEY)
    }

    override fun observeViewModel() {
        observe(viewModel.newsItem, ::initializeView)
    }

    override fun initializeViewModel() {
        viewModel = viewModelFactory.create(viewModel::class.java)
    }

    private fun initializeView(newsItem: NewsItem) {
        binding.tvTitle.text = newsItem.title
        binding.tvDescription.text = newsItem.abstractInfo
        if (!newsItem.multimedia.isNullOrEmpty()) {
//            Picasso.get().load(newsItem.multimedia.last().url).placeholder(R.color.color_white_bg)
//                    .into(binding.ivNewsMainImage)
        }
    }
}
