package com.mukundan.breathe_mindfulbreathingapp.ui.dialogs;

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.databinding.FeedbackDialogBinding
import com.mukundan.breathe_mindfulbreathingapp.utils.ThemeHelper


/**
 * Created by rahul on 1/15/2018.
 */


public class FeedbackDialog(mContext: Context?, activity: Activity) :
    Dialog(mContext!!, R.style.mydialog) {

    private lateinit var binding: FeedbackDialogBinding

    var mcontext: Context? = null
    var mActivity: Activity? = null

    init {
        this.mcontext = mContext
        this.mActivity = activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding = FeedbackDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViews()

    }


    interface OkListener {
        fun onClicked(matchid: String?)
    }

    private fun initViews() {
        changeIconsColors()
    }

    private fun changeIconsColors() {
        UiHelper.changeIconsColorsToBlue(binding.logoIv)

        if (ThemeHelper.isDarkTheme(mActivity!!)) {
            UiHelper.changeIconsColorsToWhite(binding.crossIv)
        } else {
            UiHelper.changeIconsColorsToBlack(binding.crossIv)
        }
    }
}