package com.mukundan.breathe_mindfulbreathingapp.ui.component.musicplay

import UiHelper
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE
import android.widget.FrameLayout
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.MusicPlayActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.EspressoIdlingResource
import com.mukundan.breathe_mindfulbreathingapp.utils.Event
import com.mukundan.breathe_mindfulbreathingapp.utils.ThemeHelper
import com.mukundan.breathe_mindfulbreathingapp.utils.customviews.CustomTextView
import com.mukundan.breathe_mindfulbreathingapp.utils.observe
import com.zhy.view.flowlayout.FlowLayout
import com.zhy.view.flowlayout.TagAdapter
import jp.wasabeef.glide.transformations.BlurTransformation
import javax.inject.Inject


/**
 * Created by rahul
 */

class MusicPlayActivity : BaseActivity() {
    private lateinit var binding: MusicPlayActivityBinding

    @Inject
    lateinit var musicPlayViewModel: MusicPlayViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var mTagAdapter: TagAdapter<String?>? = null
    private var mInflater: LayoutInflater? = null

    private var languagesList = ArrayList<String?>()
    private var selectedPosition: Int = 0

    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = MusicPlayActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        musicPlayViewModel = viewModelFactory.create(MusicPlayViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeIconsColors()
        setupInsets()


        languagesList.add("English")
        languagesList.add("Hindi")
        languagesList.add("Marathi")
        languagesList.add("Tamil")
        languagesList.add("German")
        languagesList.add("Russian")

        initPlayers()
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(musicPlayViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }


    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        musicPlayViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        musicPlayViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { musicPlayViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(musicPlayViewModel.newsLiveData, ::handleNewsList)
        observe(musicPlayViewModel.newsSearchFound, ::showSearchResult)
        observe(musicPlayViewModel.noSearchFound, ::noSearchResult)
        observeSnackBarMessages(musicPlayViewModel.showSnackBar)
        observeToast(musicPlayViewModel.showToast)

    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MusicPlayActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide

    }

    private fun changeIconsColors() {

        Glide.with(this).load(R.drawable.live_bg)
            .apply(RequestOptions.bitmapTransform(BlurTransformation(12, 3)))
            .into(binding.backgroundIv)

        UiHelper.changeIconsColorsToBlue(binding.playerLayout.playPauseBtn)
        UiHelper.changeIconsColorsToRed(binding.heartIv)

        if(ThemeHelper.isDarkTheme(this)){

            UiHelper.changeIconsColorsToWhite(binding.playerLayout.ivDownload)
            UiHelper.changeIconsColorsToWhite(binding.playerLayout.forwardBtn)
            UiHelper.changeIconsColorsToWhite(binding.playerLayout.ivShare)
            UiHelper.changeIconsColorsToWhite(binding.playerLayout.rewindBtn)
            UiHelper.changeIconsColorsToWhite(binding.timerIV)


        }
        UiHelper.changeIconsColorsToBlue(binding.playerLayout.playPauseBtn)

    }

    private fun setupInsets() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.topLayout.systemUiVisibility = SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        } else {
//            moviesRecyclerView.updatePadding(top = toolbarHeight + baseMoviesPadding)
        }
        ViewCompat.setOnApplyWindowInsetsListener(
            binding.topLayout
        ) { v: View?, insets: WindowInsetsCompat ->
            val params = binding.topLayout.layoutParams as FrameLayout.LayoutParams
            params.setMargins(0, 0, 0, insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
    }

    private fun initPlayers() {
        mInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mTagAdapter = object : TagAdapter<String?>(languagesList) {
            override fun getView(
                parent: FlowLayout,
                position: Int,
                s: String?
            ): View {
                val v = mInflater?.inflate(R.layout.inflater_lang, null, false)
                (v?.findViewById<CustomTextView>(R.id.inflater_text) as CustomTextView).setText(
                    languagesList.get(position)
                )

                if (selectedPosition == position) {
                    v?.findViewById<CustomTextView>(R.id.inflater_text).background =
                        ContextCompat.getDrawable(parent.context, R.drawable.rectangular_red_bg)
                } else {
                    v?.findViewById<CustomTextView>(R.id.inflater_text).background =
                        ContextCompat.getDrawable(parent.context, R.drawable.rectangular_blue_bg)

                }
                v?.findViewById<CustomTextView>(R.id.inflater_text).setOnClickListener {
                    selectedPosition = position
                    notifyDataChanged()
                }

                return v
            }
        }
        binding.playerLayout.languageAvailableLayout.setAdapter(mTagAdapter);
    }
}
