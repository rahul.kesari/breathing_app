package com.mukundan.breathe_mindfulbreathingapp.ui.adapters.pageradapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import java.util.*


class HomePagerAdapter(fm: FragmentManager?) :
    FragmentStatePagerAdapter(fm!!) {
    private val mFragmentList: MutableList<Fragment> =
        ArrayList()
    private val mFragmentTitleList: MutableList<String> =
        ArrayList()
    private var loading = false
    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun setLoaded(isLoaded: Boolean) {
        loading = isLoaded
    }

    fun addFragment(
        fragment: Fragment,
        title: String
    ) {
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

    fun removeFragment(
        fragment: Fragment,
        title: String
    ) {
        mFragmentList.remove(fragment)
        mFragmentTitleList.remove(title)
        notifyDataSetChanged()
    }

    fun clear() {
        mFragmentList.clear()
        mFragmentTitleList.clear()
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }
}

