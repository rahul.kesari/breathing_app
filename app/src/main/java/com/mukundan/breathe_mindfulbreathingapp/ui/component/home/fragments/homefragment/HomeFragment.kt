package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.homefragment

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.HomeDataModel
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.MeditationItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.RelaxeItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.SleepItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.HomeFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.HomeAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.livesession.LiveSessionActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.startbreathing.StartbreathingActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.ThemeHelper
import dagger.android.support.AndroidSupportInjection
import jp.wasabeef.glide.transformations.BlurTransformation
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var homeViewModel: HomeViewModel

    private lateinit var binding: HomeFragmentBinding


    override val layoutId: Int
        get() = R.layout.home_fragment


    override fun initViewModel() {
        homeViewModel = viewModelFactory.create(HomeViewModel::class.java)
    }

    companion object {
        fun newInstance() = HomeFragment().apply {
        }
    }


    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = HomeFragmentBinding.bind(view)
        initViewModel()
        initRecycler()
        initViews()
    }


    private fun initViews() {
        binding.breatheBtn.setOnClickListener {
            StartbreathingActivity.start(activity!!)
        }
        binding.parentCard.parentCard.setOnClickListener{
            LiveSessionActivity.start(activity!!)
        }

    }

    private fun initRecycler() {

        var dataModel = HomeDataModel()
        dataModel.meditationItems =
            listOf(MeditationItem(), MeditationItem(), MeditationItem(), MeditationItem())
        dataModel.sleepItems = listOf(SleepItem(), SleepItem(), SleepItem(), SleepItem())
        dataModel.relaxeItem = listOf(RelaxeItem(), RelaxeItem(), RelaxeItem(), RelaxeItem())

        val homeAdapter =
            HomeAdapter(homeViewModel, viewTypeList = listOf(0, 1, 2), data = dataModel)

        binding.homeRv.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
        binding.homeRv.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        binding.homeRv.adapter = ScaleInAnimationAdapter(homeAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }

    }
}
