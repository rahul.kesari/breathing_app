package com.mukundan.breathe_mindfulbreathingapp.ui.component.moodchart

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.applandeo.materialcalendarview.EventDay
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.MoodChartActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.dialogs.FeedbackDialog
import com.mukundan.breathe_mindfulbreathingapp.utils.*
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants.INSTANCE.NEWS_ITEM_KEY
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


/**
 * Created by rahul
 */

class MoodChartActivity : BaseActivity() {
    private lateinit var binding: MoodChartActivityBinding

    @Inject
    lateinit var moodchartViewModel: MoodChartViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var events: ArrayList<EventDay>

    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = MoodChartActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        moodchartViewModel = viewModelFactory.create(MoodChartViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeIconsColors()
        initCalendar()
        setupWindowAnimations()
        initViews()
    }


    private fun initViews(){
        binding.checkinBtn.setOnClickListener {
            val feedbackDialog=FeedbackDialog(this,this)
            feedbackDialog.show()
        }
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(moodchartViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }

    private fun navigateToDetailsScreen(navigateEvent: Event<NewsItem>) {
        navigateEvent.getContentIfNotHandled()?.let {
            val nextScreenIntent = Intent(this, DetailsActivity::class.java).apply {
                putExtra(NEWS_ITEM_KEY, it)
            }
            startActivity(nextScreenIntent)
        }
    }

    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        moodchartViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        moodchartViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { moodchartViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(moodchartViewModel.newsLiveData, ::handleNewsList)
        observe(moodchartViewModel.newsSearchFound, ::showSearchResult)
        observe(moodchartViewModel.noSearchFound, ::noSearchResult)
        observeEvent(moodchartViewModel.openNewsDetails, ::navigateToDetailsScreen)
        observeSnackBarMessages(moodchartViewModel.showSnackBar)
        observeToast(moodchartViewModel.showToast)

    }

    private fun changeIconsColors() {

        if (ThemeHelper.isDarkTheme(this)) {
            UiHelper.changeIconsColorsToWhite(binding.backIv)
        } else {
            UiHelper.changeIconsColorsToBlack(binding.backIv)
        }
    }
    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MoodChartActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide
    }

    private fun initCalendar() {

        events = ArrayList()


        events.add(EventDay(getCalendarDate(2020, 5, 2), R.drawable.very_bad_bg))
        events.add(EventDay(getCalendarDate(2020, 5, 1), R.drawable.very_happy_bg))
        events.add(EventDay(getCalendarDate(2020, 4, 31), R.drawable.happy_bg))
        events.add(EventDay(getCalendarDate(2020, 4, 30), R.drawable.cant_say_bg))
        events.add(EventDay(getCalendarDate(2020, 4, 29), R.drawable.okay_bg))

        binding.eventsCalendar.setEvents(events)


        val currentCalendar = Calendar.getInstance()
        currentCalendar.timeInMillis = System.currentTimeMillis()
        binding.eventsCalendar.setMaximumDate(currentCalendar)
    }

    private fun getCalendarDate(year: Int, month: Int, day: Int): Calendar {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, day)
        return calendar
    }
}
