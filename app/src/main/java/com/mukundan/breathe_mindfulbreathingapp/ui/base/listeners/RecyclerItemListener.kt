package com.mukundan.breathe_mindfulbreathingapp.ui.base.listeners

import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem

/**
 * Created by AhmedEltaher
 */

interface RecyclerItemListener {
    fun onItemSelected(newsItem: NewsItem)
}
