package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.databinding.HomeFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.RelaxeFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.pageradapter.HomePagerAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe.relaxechildfragments.RelaxeChildFragment
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class RelaxeFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var homeViewModel: RelaxeViewModel

    private lateinit var binding: RelaxeFragmentBinding

    private var view = null

    override val layoutId: Int
        get() = R.layout.relaxe_fragment

    override fun initViewModel() {
        homeViewModel = viewModelFactory.create(RelaxeViewModel::class.java)
    }

    companion object {
        fun newInstance() = RelaxeFragment().apply {
        }
    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = RelaxeFragmentBinding.bind(view)
        initViewModel()
        initViews()
    }



    private fun initViews() {

        val adapter = HomePagerAdapter(childFragmentManager)
        adapter.addFragment(RelaxeChildFragment.newInstance(true),"All")
        adapter.addFragment(RelaxeChildFragment.newInstance(true),"My Favourite")
        adapter.addFragment(RelaxeChildFragment.newInstance(true),"Beginner")
        adapter.addFragment(RelaxeChildFragment.newInstance(true),"Mysterious")
        adapter.addFragment(RelaxeChildFragment.newInstance(true),"Stress")
        adapter.addFragment(RelaxeChildFragment.newInstance(true),"All")

        binding.viewPager.adapter = adapter
        binding.tabLayout.setViewPager(binding.viewPager)
    }
}
