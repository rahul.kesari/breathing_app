package com.mukundan.breathe_mindfulbreathingapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.ProgressItemBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.SleepItemBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.viewholder.ProgressViewHolder
import com.mukundan.breathe_mindfulbreathingapp.ui.base.listeners.RecyclerItemListener
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.meditationchildfragments.MeditationChildViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe.relaxechildfragments.RelaxeChildViewModel

/**
 * Created by rahul
 */

class RelaxeChildAdapter(
    private val relaxeChildViewModel: RelaxeChildViewModel,
    private val meditations: List<NewsItem>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private val onItemClickListener: RecyclerItemListener = object : RecyclerItemListener {
        override fun onItemSelected(meditationsItem: NewsItem) {
//            relaxeChildViewModel.openNewsDetails(meditationsItem)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                SleepItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return MeditationViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MeditationViewHolder) {
            (holder as MeditationViewHolder).bind(meditations[position], onItemClickListener)
        }else{
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return meditations.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (meditations.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class MeditationViewHolder(private val itemBinding: SleepItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(meditationsItem: NewsItem, recyclerItemListener: RecyclerItemListener) {
//            itemBinding.tvCaption.text = meditationsItem.abstractInfo
//            itemBinding.tvTitle.text = meditationsItem.title
//
//            if (meditationsItem.multimedia.size > 3) {
//                val url: String? = meditationsItem.multimedia[3].url
//                Picasso.get().load(url).placeholder(R.color.color_placeholder)
//                    .error(R.color.color_placeholder).into(itemBinding.ivNewsItemImage)
//            }
//            itemBinding.rlNewsItem.setOnClickListener { recyclerItemListener.onItemSelected(meditationsItem) }
        }
    }


}

