package com.mukundan.breathe_mindfulbreathingapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.*
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.HomeDataModel
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.HomeHeaderItemBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.ProgressItemBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.viewholder.ProgressViewHolder
import com.mukundan.breathe_mindfulbreathingapp.ui.base.listeners.RecyclerItemListener
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.homefragment.HomeViewModel
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator

/**
 * Created by rahul
 */

class HomeAdapter(
    private val homeViewModell: HomeViewModel,
    private val viewTypeList: List<Int>,
    private val data: HomeDataModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_PROG = -1
        const val VIEW_ITEM_SLEEP = 0
        const val VIEW_ITEM_MEDITATION = 1
        const val VIEW_ITEM_RELAXE = 2
    }

    private val onItemClickListener: RecyclerItemListener = object : RecyclerItemListener {
        override fun onItemSelected(viewTypeListItem: NewsItem) {
//            homeViewModell.openNewsDetails(viewTypeListItem)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_ITEM_SLEEP || viewType == VIEW_ITEM_MEDITATION || viewType == VIEW_ITEM_RELAXE) {
            val itemBinding =
                HomeHeaderItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return HeaderViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            if (viewTypeList[position] == VIEW_ITEM_RELAXE) {
                holder.bind("Relaxe", data.relaxeItem, onItemClickListener)
            } else if (viewTypeList[position] == VIEW_ITEM_MEDITATION) {
                holder.bind("Meditation", data.meditationItems, onItemClickListener)
            } else if (viewTypeList[position] == VIEW_ITEM_SLEEP) {
                holder.bind("Sleep", data.sleepItems, onItemClickListener)
            }

        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return viewTypeList.size
    }


    override fun getItemViewType(position: Int): Int {
        if (viewTypeList.get(position) == null)
            return VIEW_PROG
        else if (viewTypeList.get(position) == VIEW_ITEM_SLEEP)
            return VIEW_ITEM_SLEEP
        else if (viewTypeList.get(position) == VIEW_ITEM_MEDITATION)
            return VIEW_ITEM_MEDITATION
        else
            return VIEW_ITEM_RELAXE
    }


    class HeaderViewHolder(private val itemBinding: HomeHeaderItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(
            title: String,
            _list: List<Any>,
            recyclerItemListener: RecyclerItemListener
        ) {
            itemBinding.titleTv.text = title

            itemBinding.dataRv.apply {
                layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
                itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
            }
            val adapter = HomeChildAdapter(_list)

            itemBinding.dataRv.adapter = ScaleInAnimationAdapter(adapter).apply {
                setFirstOnly(true)
                setDuration(500)
                setInterpolator(OvershootInterpolator(.5f))
            }


        }
    }


}

