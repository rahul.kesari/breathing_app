package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.SleepFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.pageradapter.HomePagerAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.sleepchildfragments.SleepChildFragment
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class SleepFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var sleepViewModell: SleepViewModel

    private lateinit var binding: SleepFragmentBinding

    override val layoutId: Int
        get() = R.layout.sleep_fragment

    override fun initViewModel() {
        sleepViewModell = viewModelFactory.create(SleepViewModel::class.java)
    }

    companion object {
        fun newInstance() = SleepFragment().apply {
        }
    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)

    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = SleepFragmentBinding.bind(view)
        initViewModel()
        initViews()
    }


    private fun initViews() {
        val adapter = HomePagerAdapter(childFragmentManager)
        adapter.addFragment(SleepChildFragment.newInstance(true), "All")
        adapter.addFragment(SleepChildFragment.newInstance(true), "My Favourite")
        adapter.addFragment(SleepChildFragment.newInstance(true), "Beginner")
        adapter.addFragment(SleepChildFragment.newInstance(true), "Mysterious")
        adapter.addFragment(SleepChildFragment.newInstance(true), "Stress")
        adapter.addFragment(SleepChildFragment.newInstance(true), "All")

        binding.viewPager.adapter = adapter
        binding.tabLayout.setViewPager(binding.viewPager)
    }

}
