package com.mukundan.breathe_mindfulbreathingapp.ui.component.login

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.LoginActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.MainActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.signup.SignupActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.*
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants.INSTANCE.NEWS_ITEM_KEY
import javax.inject.Inject


/**
 * Created by rahul
 */

class LoginActivity : BaseActivity() {
    private lateinit var binding: LoginActivityBinding

    @Inject
    lateinit var loginViewModel: LoginViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = LoginActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        loginViewModel = viewModelFactory.create(LoginViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.signupBtn.setOnClickListener {
            SignupActivity.start(this)
        }
        binding.loginBtn.setOnClickListener {
            MainActivity.start(this)
        }
        changeIconsColors()
        setupWindowAnimations()
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(loginViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }

    private fun navigateToDetailsScreen(navigateEvent: Event<NewsItem>) {
        navigateEvent.getContentIfNotHandled()?.let {
            val nextScreenIntent = Intent(this, DetailsActivity::class.java).apply {
                putExtra(NEWS_ITEM_KEY, it)
            }
            startActivity(nextScreenIntent)
        }
    }

    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        loginViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        loginViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { loginViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(loginViewModel.newsLiveData, ::handleNewsList)
        observe(loginViewModel.newsSearchFound, ::showSearchResult)
        observe(loginViewModel.noSearchFound, ::noSearchResult)
        observeEvent(loginViewModel.openNewsDetails, ::navigateToDetailsScreen)
        observeSnackBarMessages(loginViewModel.showSnackBar)
        observeToast(loginViewModel.showToast)

    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, LoginActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide

    }

    private fun changeIconsColors() {
        if (ThemeHelper.isDarkTheme(this)) {
            UiHelper.changeIconsColorsToWhite(binding.ivMessage)
            UiHelper.changeIconsColorsToWhite(binding.ivPass)
            UiHelper.changeIconsColorsToWhite(binding.ivFb)
            UiHelper.changeIconsColorsToWhite(binding.ivGoogle)
            UiHelper.changeIconsColorsToWhite(binding.ivTwitter)

        } else {
            UiHelper.changeIconsColorsToBlack(binding.ivMessage)
            UiHelper.changeIconsColorsToBlack(binding.ivPass)
            UiHelper.changeIconsColorsToBlack(binding.ivFb)
            UiHelper.changeIconsColorsToBlack(binding.ivGoogle)
            UiHelper.changeIconsColorsToBlack(binding.ivTwitter)
        }
    }
}
