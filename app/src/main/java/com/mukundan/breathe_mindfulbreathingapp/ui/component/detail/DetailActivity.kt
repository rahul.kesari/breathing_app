package com.mukundan.breathe_mindfulbreathingapp.ui.component.detail

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import android.util.Log
import android.view.ViewTreeObserver
import android.view.animation.OvershootInterpolator
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.test.espresso.IdlingResource
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.DetailActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.LessonAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.*
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import javax.inject.Inject


/**
 * Created by rahul
 */

class DetailActivity : BaseActivity() {
    private lateinit var binding: DetailActivityBinding

    @Inject
    lateinit var detailViewModel: DetailViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var lessons = ArrayList<NewsItem>()


    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = DetailActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        detailViewModel = viewModelFactory.create(DetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeIconsColors()
        bindListData()
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(detailViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }


    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        detailViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        detailViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { detailViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(detailViewModel.newsLiveData, ::handleNewsList)
        observe(detailViewModel.newsSearchFound, ::showSearchResult)
        observe(detailViewModel.noSearchFound, ::noSearchResult)
        observeSnackBarMessages(detailViewModel.showSnackBar)
        observeToast(detailViewModel.showToast)

    }

    companion object {

        var TAG = DetailActivity.javaClass.canonicalName
        fun start(context: Context) {
            context.startActivity(Intent(context, DetailActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide
    }

    private fun changeIconsColors() {

        Glide.with(this).load(R.drawable.live_bg)
            .apply(
                RequestOptions.bitmapTransform(
                    RoundedCornersTransformation(
                        20,
                        20,
                        RoundedCornersTransformation.CornerType.BOTTOM
                    )
                ).centerCrop()
            )
            .into(binding.bgIV)

        if (ThemeHelper.isDarkTheme(this)) {
            UiHelper.changeIconsColorsToWhite(binding.backIv)
        } else {
            UiHelper.changeIconsColorsToBlack(binding.backIv)
        }
    }

    private fun bindListData() {

        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())
        lessons.add(NewsItem())

        val newsAdapter = LessonAdapter(lessons)

        binding.lessonsRV.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
        binding.lessonsRV.layoutManager = LinearLayoutManager(this)
        // binding.sleepListRv.addItemDecoration(GridSpacingItemDecoration(2, 5, true))

        binding.lessonsRV.adapter = ScaleInAnimationAdapter(newsAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }

        binding.topCardRl.getViewTreeObserver()
            .addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    L.d(TAG!!, " topCard Height>>>>" + binding.topCardRl.measuredHeight)
//                    mediaIV.getViewTreeObserver().removeOnPreDrawListener(this)
                    binding.viewOne.layoutParams.height = binding.topCardRl.measuredHeight
                    binding.viewOne.requestLayout()
                    return true
                }
            })

        binding.bottomCardRl.getViewTreeObserver()
            .addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    L.d(TAG!!, " bottomCard Height>>>>" + binding.bottomCardRl.measuredHeight)
//                    mediaIV.getViewTreeObserver().removeOnPreDrawListener(this)
                    binding.viewTwo.layoutParams.height = binding.bottomCardRl.measuredHeight
                    binding.viewTwo.requestLayout()
                    return true
                }
            })
    }
}
