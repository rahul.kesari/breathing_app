package com.mukundan.breathe_mindfulbreathingapp.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.*
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.viewholder.ProgressViewHolder
import com.mukundan.breathe_mindfulbreathingapp.ui.base.listeners.RecyclerItemListener
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.meditationchildfragments.MeditationChildViewModel
import com.mukundan.breathe_mindfulbreathingapp.ui.component.musicplay.MusicPlayActivity
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

/**
 * Created by rahul
 */

class RemindersListAdapter(private val datas: List<Any>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context

    private val onItemClickListener: RecyclerItemListener = object : RecyclerItemListener {
        override fun onItemSelected(datasItem: NewsItem) {
//            meditationChildViewModell.openNewsDetails(datasItem)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                ReminderItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return RemindersViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RemindersViewHolder) {
            (holder as RemindersViewHolder).bind(datas[position], onItemClickListener, mContext)
        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class RemindersViewHolder(private val itemBinding: ReminderItemListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(datasItem: Any, recyclerItemListener: RecyclerItemListener, context: Context) {

          
        }
    }


}

