package com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.MelodiesActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.MusicPlayActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.pageradapter.HomePagerAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies.fragments.MelodiesFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.dialogs.TimerDialog
import com.mukundan.breathe_mindfulbreathingapp.utils.EspressoIdlingResource
import com.mukundan.breathe_mindfulbreathingapp.utils.Event
import com.mukundan.breathe_mindfulbreathingapp.utils.ThemeHelper
import com.mukundan.breathe_mindfulbreathingapp.utils.observe
import javax.inject.Inject


/**
 * Created by rahul
 */

class MelodiesActivity : BaseActivity() {
    private lateinit var binding: MelodiesActivityBinding

    @Inject
    lateinit var melodiesViewModel: MelodiesViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = MelodiesActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        melodiesViewModel = viewModelFactory.create(MelodiesViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeIconsColors()
        initViews()
        initListeners()
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(melodiesViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }


    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        melodiesViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        melodiesViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { melodiesViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(melodiesViewModel.newsLiveData, ::handleNewsList)
        observe(melodiesViewModel.newsSearchFound, ::showSearchResult)
        observe(melodiesViewModel.noSearchFound, ::noSearchResult)
        observeSnackBarMessages(melodiesViewModel.showSnackBar)
        observeToast(melodiesViewModel.showToast)

    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MelodiesActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide

    }

    private fun changeIconsColors() {
        if (ThemeHelper.isDarkTheme(this)) {
            UiHelper.changeIconsColorsToWhite(binding.backIv)
            UiHelper.changeIconsColorsToWhite(binding.timerIv)

        } else {
            UiHelper.changeIconsColorsToBlack(binding.backIv)
            UiHelper.changeIconsColorsToBlack(binding.timerIv)
        }
    }


    private fun initViews() {
        val adapter = HomePagerAdapter(supportFragmentManager)
        adapter.addFragment(MelodiesFragment.newInstance(true), "All")
        adapter.addFragment(MelodiesFragment.newInstance(true), "My Favourite")
        adapter.addFragment(MelodiesFragment.newInstance(true), "Beginner")
        adapter.addFragment(MelodiesFragment.newInstance(true), "Mysterious")
        adapter.addFragment(MelodiesFragment.newInstance(true), "Stress")
        adapter.addFragment(MelodiesFragment.newInstance(true), "All")

        binding.viewPager.adapter = adapter
        binding.tabLayout.setViewPager(binding.viewPager)
    }

    private fun initListeners() {
        binding.timerIv.setOnClickListener {
            showTimerDialog()
        }
    }

    private fun showTimerDialog() {
        val timerDialog = TimerDialog(this)
        timerDialog.show()

    }


}
