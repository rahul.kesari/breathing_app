package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.profile

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.databinding.HomeFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.ProfileFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.moodchart.MoodChartActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.settings.SettingsActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.L
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var homeViewModel: ProfileViewModel

    private lateinit var binding: ProfileFragmentBinding

    override val layoutId: Int
        get() = R.layout.profile_fragment

    override fun initViewModel() {
        homeViewModel = viewModelFactory.create(ProfileViewModel::class.java)
    }

    companion object {
        var TAG: String = ProfileFragment.javaClass.canonicalName!!

        fun newInstance() = ProfileFragment().apply {
        }
    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)

    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = ProfileFragmentBinding.bind(view)
        initViewModel()
        initViews()
        changeIconsColors()
    }

    private fun changeIconsColors() {
        UiHelper.changeIconsColorsToWhite(binding.homeIv)
        UiHelper.changeIconsColorsToWhite(binding.sleepIv)
        UiHelper.changeIconsColorsToWhite(binding.relaxeIv)
        UiHelper.changeIconsColorsToWhite(binding.meditationIv)
        UiHelper.changeIconsColorsToWhite(binding.shareIv)
    }

    private fun initViews() {
        binding.moodchartBtn.setOnClickListener {
            MoodChartActivity.start(activity!!)
        }
        binding.settingsBtn.setOnClickListener {
            SettingsActivity.start(activity!!)
        }
    }
}
