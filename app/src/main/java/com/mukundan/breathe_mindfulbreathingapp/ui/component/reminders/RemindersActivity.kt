package com.mukundan.breathe_mindfulbreathingapp.ui.component.reminders

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.IdlingResource
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.RemindersActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.RemindersListAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.dialogs.AddReminderDialog
import com.mukundan.breathe_mindfulbreathingapp.utils.*
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants.INSTANCE.NEWS_ITEM_KEY
import com.yarolegovich.discretescrollview.DSVOrientation
import com.yarolegovich.discretescrollview.DiscreteScrollView.OnItemChangedListener
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import javax.inject.Inject


/**
 * Created by rahul
 */

class RemindersActivity : BaseActivity(), OnItemChangedListener<RecyclerView.ViewHolder> {
    private lateinit var binding: RemindersActivityBinding

    @Inject
    lateinit var remindersViewModel: RemindersViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var mRemindersAdapter: RemindersListAdapter
    private lateinit var infiniteAdapter: InfiniteScrollAdapter<RecyclerView.ViewHolder>


    private var remindersList = ArrayList<NewsItem>()


    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = RemindersActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        remindersViewModel = viewModelFactory.create(RemindersViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupWindowAnimations()
        initViews()
        initList()
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(remindersViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }

    private fun navigateToDetailsScreen(navigateEvent: Event<NewsItem>) {
        navigateEvent.getContentIfNotHandled()?.let {
            val nextScreenIntent = Intent(this, DetailsActivity::class.java).apply {
                putExtra(NEWS_ITEM_KEY, it)
            }
            startActivity(nextScreenIntent)
        }
    }

    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        remindersViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        remindersViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { remindersViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(remindersViewModel.newsLiveData, ::handleNewsList)
        observe(remindersViewModel.newsSearchFound, ::showSearchResult)
        observe(remindersViewModel.noSearchFound, ::noSearchResult)
        observeEvent(remindersViewModel.openNewsDetails, ::navigateToDetailsScreen)
        observeSnackBarMessages(remindersViewModel.showSnackBar)
        observeToast(remindersViewModel.showToast)

    }

    companion object {
        val TAG = RemindersListAdapter.javaClass.canonicalName
        fun start(context: Context) {
            context.startActivity(Intent(context, RemindersActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide
    }

    private fun initViews() {
        UiHelper.changeIconsColorsToWhite(binding.shareIv)
        binding.addReminderBtn.setOnClickListener {
            openAddReminderDialog()
        }

        if (ThemeHelper.isDarkTheme(this)) {
            UiHelper.changeIconsColorsToWhite(binding.backIv)
        } else {
            UiHelper.changeIconsColorsToBlack(binding.backIv)
        }
    }

    private fun initList() {
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())
        remindersList.add(NewsItem())

        mRemindersAdapter = RemindersListAdapter(remindersList)

        binding.viewPager.setOrientation(DSVOrientation.HORIZONTAL)
        binding.viewPager.addOnItemChangedListener(this)
        infiniteAdapter = InfiniteScrollAdapter.wrap(mRemindersAdapter)
        binding.viewPager.setAdapter(infiniteAdapter)
        binding.viewPager.setItemTransitionTimeMillis(100)
        binding.viewPager.setSlideOnFling(false)
        binding.viewPager.setItemTransformer(
            ScaleTransformer.Builder()
                .setMinScale(0.7f)
                .build()
        )
    }

    override fun onCurrentItemChanged(holder: RecyclerView.ViewHolder?, adapterPosition: Int) {

        L.d(TAG!!, "selected Position>>>>" + infiniteAdapter.getRealPosition(adapterPosition))
        val positionInDataSet: Int = infiniteAdapter.getRealPosition(adapterPosition)
        when (positionInDataSet) {
            0 -> {
            }
        }
    }

    private fun openAddReminderDialog() {
        val reminderDialog = AddReminderDialog(this)
        reminderDialog.show()
    }
}
