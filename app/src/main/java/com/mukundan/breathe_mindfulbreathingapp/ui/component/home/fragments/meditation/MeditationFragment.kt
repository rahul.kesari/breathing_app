package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.databinding.MeditationFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.pageradapter.HomePagerAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.meditationchildfragments.MeditationChildFragment
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class MeditationFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var meditationViewModel: MeditationViewModel

    private lateinit var binding: MeditationFragmentBinding

    private var view = null

    override val layoutId: Int
        get() = R.layout.meditation_fragment


    override fun initViewModel() {
        meditationViewModel = viewModelFactory.create(MeditationViewModel::class.java)
    }

    companion object {
        fun newInstance() = MeditationFragment().apply {
        }
    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)

    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MeditationFragmentBinding.bind(view)
        initViewModel()
        initViews()
    }


    private fun initViews() {

        val adapter = HomePagerAdapter(childFragmentManager)
        adapter.addFragment(MeditationChildFragment.newInstance(true),"All")
        adapter.addFragment(MeditationChildFragment.newInstance(true),"My Favourite")
        adapter.addFragment(MeditationChildFragment.newInstance(true),"Beginner")
        adapter.addFragment(MeditationChildFragment.newInstance(true),"Mysterious")
        adapter.addFragment(MeditationChildFragment.newInstance(true),"Stress")
        adapter.addFragment(MeditationChildFragment.newInstance(true),"All")

        binding.viewPager.adapter = adapter
        binding.tabLayout.setViewPager(binding.viewPager)
    }
}
