package com.mukundan.breathe_mindfulbreathingapp.ui.component.details

import androidx.lifecycle.MutableLiveData
import com.mukundan.breathe_mindfulbreathingapp.data.error.mapper.ErrorMapper
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseViewModel
import com.mukundan.breathe_mindfulbreathingapp.usecase.errors.ErrorManager
import javax.inject.Inject

/**
 * Created by AhmedEltaher
 */

class DetailsViewModel @Inject
constructor() : BaseViewModel() {
    var newsItem: MutableLiveData<NewsItem> = MutableLiveData()
    override val errorManager: ErrorManager
        get() = ErrorManager(ErrorMapper())
}
