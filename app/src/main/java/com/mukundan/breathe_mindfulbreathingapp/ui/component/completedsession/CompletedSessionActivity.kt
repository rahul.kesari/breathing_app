package com.mukundan.breathe_mindfulbreathingapp.ui.component.completedsession

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.CompletedSessionActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.LoginActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.home.MainActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.signup.SignupActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.*
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants.INSTANCE.NEWS_ITEM_KEY
import javax.inject.Inject


/**
 * Created by rahul
 */

class CompletedSessionActivity : BaseActivity() {
    private lateinit var binding: CompletedSessionActivityBinding

    @Inject
    lateinit var completedSessionViewModel: CompletedSessionViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = CompletedSessionActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        completedSessionViewModel = viewModelFactory.create(CompletedSessionViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeIconsColors()

    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(completedSessionViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }



    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        completedSessionViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        completedSessionViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { completedSessionViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(completedSessionViewModel.newsLiveData, ::handleNewsList)
        observe(completedSessionViewModel.newsSearchFound, ::showSearchResult)
        observe(completedSessionViewModel.noSearchFound, ::noSearchResult)
        observeSnackBarMessages(completedSessionViewModel.showSnackBar)
        observeToast(completedSessionViewModel.showToast)

    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, CompletedSessionActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide

    }

    private fun changeIconsColors() {
        UiHelper.changeIconsColorsToWhite(binding.shareIv)
        UiHelper.changeIconsColorsToBlue(binding.logoIv)

        if(ThemeHelper.isDarkTheme(this)){
            UiHelper.changeIconsColorsToWhite(binding.backIv)
        }else{
            UiHelper.changeIconsColorsToBlack(binding.backIv)
        }
    }
}
