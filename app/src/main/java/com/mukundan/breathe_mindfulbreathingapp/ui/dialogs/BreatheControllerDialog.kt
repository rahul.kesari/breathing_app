package com.mukundan.breathe_mindfulbreathingapp.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window

import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.ui.component.startbreathing.StartbreathingActivity

class BreatheControllerDialog {
    private var mContext: StartbreathingActivity? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    var bottomSheetView: View? = null
    private var mListener: UpdateListener? = null

    fun setUpDialog(
        context: StartbreathingActivity,
        mListener: UpdateListener
    ) {
        mContext = context
        this.mListener = mListener
        bottomSheetView = context.getLayoutInflater().inflate(R.layout.breathe_controller_dialog, null)
        bottomSheetDialog = BottomSheetDialog(context,R.style.mydialog)
        bottomSheetDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bottomSheetDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        bottomSheetDialog!!.setContentView(bottomSheetView!!)
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView!!.parent as View)
        //        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);

    }

    fun hideDialog() {
        if (bottomSheetDialog != null) {
            if (bottomSheetDialog!!.isShowing) bottomSheetDialog!!.dismiss()
        }
    }

    fun showDialog() {
        if (bottomSheetDialog != null) {
            if (!((mContext as StartbreathingActivity)?.isFinishing())) {
                bottomSheetDialog!!.show()
            }
        }
    }

    interface UpdateListener {
        fun onResetClicked()
    }
}
