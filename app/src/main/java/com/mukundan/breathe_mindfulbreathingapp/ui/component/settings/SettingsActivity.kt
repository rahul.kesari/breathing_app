package com.mukundan.breathe_mindfulbreathingapp.ui.component.settings

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.transition.Transition
import android.transition.TransitionInflater
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.applandeo.materialcalendarview.EventDay
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.MoodChartActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.SettingsActivittyBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.accountsettings.AccountSettingsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.reminders.RemindersActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.*
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants.INSTANCE.NEWS_ITEM_KEY
import org.greenrobot.eventbus.EventBus
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


/**
 * Created by rahul
 */

class SettingsActivity : BaseActivity() {
    private lateinit var binding: SettingsActivittyBinding

    @Inject
    lateinit var settingsViewModel: SettingsViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = SettingsActivittyBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        settingsViewModel = viewModelFactory.create(SettingsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupWindowAnimations()
        changeIconsColors()
        initViews()
    }


    private fun changeIconsColors() {
        if (ThemeHelper.isDarkTheme(this)) {
            UiHelper.changeIconsColorsToWhite(binding.backIv)
        } else {
            UiHelper.changeIconsColorsToBlack(binding.backIv)
        }
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(settingsViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }

    private fun navigateToDetailsScreen(navigateEvent: Event<NewsItem>) {
        navigateEvent.getContentIfNotHandled()?.let {
            val nextScreenIntent = Intent(this, DetailsActivity::class.java).apply {
                putExtra(NEWS_ITEM_KEY, it)
            }
            startActivity(nextScreenIntent)
        }
    }

    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        settingsViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        settingsViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { settingsViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(settingsViewModel.newsLiveData, ::handleNewsList)
        observe(settingsViewModel.newsSearchFound, ::showSearchResult)
        observe(settingsViewModel.noSearchFound, ::noSearchResult)
        observeEvent(settingsViewModel.openNewsDetails, ::navigateToDetailsScreen)
        observeSnackBarMessages(settingsViewModel.showSnackBar)
        observeToast(settingsViewModel.showToast)

    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, SettingsActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide
    }

    private fun initViews() {
        binding.accountBtn.setOnClickListener {
            AccountSettingsActivity.start(this)
        }
        binding.remindersRl.setOnClickListener {
            RemindersActivity.start(this)
        }

        binding.darkSwitchBtn.isChecked = Prefs.getString(Constants.PREFS.CURRENT_STYLE)!!.equals(ThemeHelper.darkMode)

        binding.darkSwitchBtn.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                ThemeHelper.applyTheme(ThemeHelper.darkMode)
                Prefs.putString(Constants.PREFS.CURRENT_STYLE, ThemeHelper.darkMode)
                EventBus.getDefault().post(ThemeHelper.darkMode)

            } else {
                ThemeHelper.applyTheme(ThemeHelper.lightMode)
                Prefs.putString(Constants.PREFS.CURRENT_STYLE, ThemeHelper.lightMode)
                EventBus.getDefault().post(ThemeHelper.lightMode)

            }
        }
    }

}
