package com.mukundan.breathe_mindfulbreathingapp.ui.component.livesession

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import android.view.View
import android.view.View.*
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.LiveSessionActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.utils.EspressoIdlingResource
import com.mukundan.breathe_mindfulbreathingapp.utils.Event
import com.mukundan.breathe_mindfulbreathingapp.utils.ThemeHelper
import com.mukundan.breathe_mindfulbreathingapp.utils.observe
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.player_controller.*
import javax.inject.Inject


/**
 * Created by rahul
 */

class LiveSessionActivity : BaseActivity() {
    private lateinit var binding: LiveSessionActivityBinding

    @Inject
    lateinit var liveSessionViewModel: LiveSessionViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = LiveSessionActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        liveSessionViewModel = viewModelFactory.create(LiveSessionViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeIconsColors()
        setupInsets()
        handlePlayerUI()
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(liveSessionViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }


    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        liveSessionViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        liveSessionViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { liveSessionViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(liveSessionViewModel.newsLiveData, ::handleNewsList)
        observe(liveSessionViewModel.newsSearchFound, ::showSearchResult)
        observe(liveSessionViewModel.noSearchFound, ::noSearchResult)
        observeSnackBarMessages(liveSessionViewModel.showSnackBar)
        observeToast(liveSessionViewModel.showToast)

    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, LiveSessionActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide

    }

    private fun changeIconsColors() {

        Glide.with(this).load(R.drawable.live_bg)
            .apply(RequestOptions.bitmapTransform(BlurTransformation(12, 3)))
            .into(binding.backgroundIv)


        if(ThemeHelper.isDarkTheme(this)){
            UiHelper.changeIconsColorsToWhite(binding.playerLayout.rewindBtn)
        }
        UiHelper.changeIconsColorsToBlue(binding.playerLayout.playPauseBtn)

    }

    private fun setupInsets() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.topLayout.systemUiVisibility = SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        } else {
//            moviesRecyclerView.updatePadding(top = toolbarHeight + baseMoviesPadding)
        }
        ViewCompat.setOnApplyWindowInsetsListener(
            binding.topLayout
        ) { v: View?, insets: WindowInsetsCompat ->
            val params = binding.topLayout.layoutParams as FrameLayout.LayoutParams
            params.setMargins(0, 0, 0, insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
    }

    private fun handlePlayerUI() {
        binding.playerLayout.availableIn.visibility = INVISIBLE
        binding.playerLayout.exoDuration.visibility = GONE
        binding.playerLayout.exoTime.visibility = GONE
        binding.playerLayout.liveView.visibility = VISIBLE

        binding.playerLayout.downloadBtn.visibility = GONE

        binding.playerLayout.forwardBtn.visibility = GONE
        binding.playerLayout.shareBtn.visibility = GONE

    }

}
