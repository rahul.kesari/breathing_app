package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.relaxe.relaxechildfragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import androidx.annotation.Nullable
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.MeditationChildFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.RelaxeChildFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.MeditationChildAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.RelaxeChildAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseFragment
import com.mukundan.breathe_mindfulbreathingapp.ui.component.melodies.MelodiesActivity
import dagger.android.support.AndroidSupportInjection
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class RelaxeChildFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var relaxeChildViewModel: RelaxeChildViewModel

    private lateinit var binding: RelaxeChildFragmentBinding

    var relalxes = ArrayList<NewsItem>()

    override val layoutId: Int
        get() = R.layout.relaxe_child_fragment

    override fun initViewModel() {
        relaxeChildViewModel = viewModelFactory.create(RelaxeChildViewModel::class.java)
    }

    companion object {

        private const val DATA = "my_data"
        fun newInstance(aBoolean: Boolean) = RelaxeChildFragment().apply {
            arguments = bundleOf(
                DATA to aBoolean
            )
        }

    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)

    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = RelaxeChildFragmentBinding.bind(view)
        initViewModel()
        initViews()
    }

    private fun initViews() {

        bindListData()
    }

    private fun bindListData() {

        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())
        relalxes.add(NewsItem())

        val newsAdapter = RelaxeChildAdapter(relaxeChildViewModel, relalxes)

        binding.relaxeListRv.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
        binding.relaxeListRv.layoutManager = GridLayoutManager(context, 2)
        // binding.sleepListRv.addItemDecoration(GridSpacingItemDecoration(2, 5, true))

        binding.relaxeListRv.adapter = ScaleInAnimationAdapter(newsAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }


        binding.melodiesBtn.setOnClickListener {
            MelodiesActivity.start(activity!!)
        }

//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//
////            showDataView(true)
//        } else {
////            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }
}
