package com.mukundan.breathe_mindfulbreathingapp.ui.component.news.newsAdapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.NewsItemBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.base.listeners.RecyclerItemListener
import com.mukundan.breathe_mindfulbreathingapp.ui.component.news.NewsListViewModel
/**
 * Created by rahul
 */

class NewsAdapter(private val newsListViewModel: NewsListViewModel, private val news: List<NewsItem>) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private val onItemClickListener: RecyclerItemListener = object : RecyclerItemListener {
        override fun onItemSelected(newsItem: NewsItem) {
            newsListViewModel.openNewsDetails(newsItem)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val itemBinding = NewsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NewsViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(news[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return news.size
    }

    class NewsViewHolder(private val itemBinding: NewsItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(newsItem: NewsItem, recyclerItemListener: RecyclerItemListener) {
            itemBinding.tvCaption.text = newsItem.abstractInfo
            itemBinding.tvTitle.text = newsItem.title

            if (newsItem.multimedia.size > 3) {
                val url: String? = newsItem.multimedia[3].url
            //    Picasso.get().load(url).placeholder(R.color.color_placeholder).error(R.color.color_placeholder).into(itemBinding.ivNewsItemImage)
            }
            itemBinding.rlNewsItem.setOnClickListener { recyclerItemListener.onItemSelected(newsItem) }
        }
    }
}

