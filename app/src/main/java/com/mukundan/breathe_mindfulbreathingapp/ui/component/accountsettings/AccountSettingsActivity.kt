package com.mukundan.breathe_mindfulbreathingapp.ui.component.accountsettings

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import androidx.annotation.RequiresApi
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.test.espresso.IdlingResource
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.Resource
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsModel
import com.mukundan.breathe_mindfulbreathingapp.databinding.AccountSettingsActivityBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.details.DetailsActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.component.managesubscription.ManageSubscriptionActivity
import com.mukundan.breathe_mindfulbreathingapp.ui.dialogs.ChangePasswordDialog
import com.mukundan.breathe_mindfulbreathingapp.ui.dialogs.NameUpdateDialog
import com.mukundan.breathe_mindfulbreathingapp.utils.*
import com.mukundan.breathe_mindfulbreathingapp.utils.Constants.INSTANCE.NEWS_ITEM_KEY
import javax.inject.Inject


/**
 * Created by rahul
 */

class AccountSettingsActivity : BaseActivity() {
    private lateinit var binding: AccountSettingsActivityBinding

    @Inject
    lateinit var accountSettingsViewModel: AccountSettingsViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    val countingIdlingResource: IdlingResource
        @VisibleForTesting
        get() = EspressoIdlingResource.idlingResource

    override fun initViewBinding() {
        binding = AccountSettingsActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        accountSettingsViewModel = viewModelFactory.create(AccountSettingsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupWindowAnimations()
        initViews()
        changeIconsColors()
    }

    private fun bindListData(newsModel: NewsModel) {
//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//            val newsAdapter = NewsAdapter(accountSettingsViewModel, newsModel.newsItems)
//            binding.rvNewsList.adapter = newsAdapter
//            showDataView(true)
//        } else {
//            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }

    private fun navigateToDetailsScreen(navigateEvent: Event<NewsItem>) {
        navigateEvent.getContentIfNotHandled()?.let {
            val nextScreenIntent = Intent(this, DetailsActivity::class.java).apply {
                putExtra(NEWS_ITEM_KEY, it)
            }
            startActivity(nextScreenIntent)
        }
    }

    private fun observeSnackBarMessages(event: LiveData<Event<Int>>) {
//        binding.rlNewsList.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
//        binding.rlNewsList.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showSearchError() {
//        accountSettingsViewModel.showSnackbarMessage(R.string.search_error)
    }

    private fun showDataView(show: Boolean) {
//        binding.tvNoData.visibility = if (show) GONE else VISIBLE
//        binding.shimmerFrameLayout.toGone()
    }

    private fun showLoadingView() {
//        binding.shimmerFrameLayout.toVisible()
//        binding.shimmerFrameLayout.startShimmer()
//        binding.tvNoData.toGone()
//        EspressoIdlingResource.increment()
    }


    private fun showSearchResult(newsItem: NewsItem) {
        accountSettingsViewModel.openNewsDetails(newsItem)
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun noSearchResult(unit: Unit) {
        showSearchError()
//        binding.shimmerFrameLayout.toGone()
//        binding.shimmerFrameLayout.stopShimmer()
    }

    private fun handleNewsList(newsModel: Resource<NewsModel>) {
        when (newsModel) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> newsModel.data?.let { bindListData(newsModel = it) }
            is Resource.DataError -> {
                showDataView(false)
                newsModel.errorCode?.let { accountSettingsViewModel.showToastMessage(it) }
            }
        }

    }

    override fun observeViewModel() {
        observe(accountSettingsViewModel.newsLiveData, ::handleNewsList)
        observe(accountSettingsViewModel.newsSearchFound, ::showSearchResult)
        observe(accountSettingsViewModel.noSearchFound, ::noSearchResult)
        observeEvent(accountSettingsViewModel.openNewsDetails, ::navigateToDetailsScreen)
        observeSnackBarMessages(accountSettingsViewModel.showSnackBar)
        observeToast(accountSettingsViewModel.showToast)

    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, AccountSettingsActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide: Transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.activity_slide)
        window.exitTransition = slide
    }

    private fun initViews() {
        binding.updateNameRL.setOnClickListener {
            showNameUpdateDialog()
        }

        binding.changePassRl.setOnClickListener {
            showChangePassDialog()
        }

        binding.subscriptionRl.setOnClickListener {
            ManageSubscriptionActivity.start(this)
        }
    }

    private fun showNameUpdateDialog() {
        val nameUpdateDialog = NameUpdateDialog(this)
        nameUpdateDialog.show()
    }

    private fun showChangePassDialog() {
        val changepassDialog = ChangePasswordDialog(this)
        changepassDialog.show()
    }


    private fun changeIconsColors() {
        if (ThemeHelper.isDarkTheme(this)) {
            UiHelper.changeIconsColorsToWhite(binding.backIv)
        } else {
            UiHelper.changeIconsColorsToBlack(binding.backIv)
        }
    }
}
