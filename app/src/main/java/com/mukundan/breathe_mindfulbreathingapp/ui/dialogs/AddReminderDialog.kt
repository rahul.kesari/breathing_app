package com.mukundan.breathe_mindfulbreathingapp.ui.dialogs;

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.databinding.AddReminderDialogBinding
import com.mukundan.breathe_mindfulbreathingapp.databinding.TimerDialogBinding


/**
 * Created by rahul on 1/15/2018.
 */


public class AddReminderDialog(context: Context?) : Dialog(context!!, R.style.mydialog) {

    private lateinit var binding: AddReminderDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding = AddReminderDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initTimers()
    }


    interface OkListener {
        fun onClicked(matchid: String?)
    }

    private fun initTimers() {
        val hoursData = arrayOfNulls<String>(25)

        for (i in 0..24) {
            if (i <= 9) {
                hoursData[i] = "0" + i
            } else {
                hoursData[i] = "" + i
            }
        }

        binding.timeHours.minValue = 1
        binding.timeHours.maxValue = hoursData.size
        binding.timeHours.displayedValues = hoursData
        binding.timeHours.value = 3

        var minutesData = arrayOfNulls<String>(61)

        for (i in 0..60) {
            if (i <= 9) {
                minutesData[i] = "0" + i
            } else {
                minutesData[i] = "" + i
            }
        }
        binding.timeMinutes.minValue = 1
        binding.timeMinutes.maxValue = minutesData.size
        binding.timeMinutes.displayedValues = minutesData
        binding.timeMinutes.value = 10


        var dotsData = arrayOf(":",":",":")

        binding.dotsScroll.minValue = 1
        binding.dotsScroll.maxValue = dotsData.size
        binding.dotsScroll.displayedValues = dotsData

        var ampmData = arrayOf("AM","PM")

        binding.ampmScroll.minValue = 1
        binding.ampmScroll.maxValue = ampmData.size
        binding.ampmScroll.displayedValues = ampmData
    }

}