package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.meditation.meditationchildfragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import androidx.annotation.Nullable
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.MeditationChildFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.MeditationChildAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseFragment
import dagger.android.support.AndroidSupportInjection
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class MeditationChildFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var meditationChildViewModell: MeditationChildViewModel

    private lateinit var binding: MeditationChildFragmentBinding

    private var meditations = ArrayList<NewsItem>()

    override val layoutId: Int
        get() = R.layout.meditation_child_fragment

    override fun initViewModel() {
        meditationChildViewModell = viewModelFactory.create(MeditationChildViewModel::class.java)
    }

    companion object {

        private const val DATA = "my_data"
        fun newInstance(aBoolean: Boolean) = MeditationChildFragment().apply {
            arguments = bundleOf(
                DATA to aBoolean
            )
        }

    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)

    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MeditationChildFragmentBinding.bind(view)
        initViewModel()
        initViews()
    }

    private fun initViews() {

        bindListData()
    }

    private fun bindListData() {

        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())
        meditations.add(NewsItem())

        val newsAdapter = MeditationChildAdapter(meditationChildViewModell, meditations)

        binding.meditationListRv.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
        binding.meditationListRv.layoutManager = GridLayoutManager(context, 2)
       // binding.sleepListRv.addItemDecoration(GridSpacingItemDecoration(2, 5, true))

        binding.meditationListRv.adapter = ScaleInAnimationAdapter(newsAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }



//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//
////            showDataView(true)
//        } else {
////            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }
}
