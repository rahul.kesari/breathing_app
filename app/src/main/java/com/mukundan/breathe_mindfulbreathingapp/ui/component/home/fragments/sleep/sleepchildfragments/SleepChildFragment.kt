package com.mukundan.breathe_mindfulbreathingapp.ui.component.home.fragments.sleep.sleepchildfragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import androidx.annotation.Nullable
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.mukundan.breathe_mindfulbreathingapp.R
import com.mukundan.breathe_mindfulbreathingapp.data.remote.dto.NewsItem
import com.mukundan.breathe_mindfulbreathingapp.databinding.SleepChildFragmentBinding
import com.mukundan.breathe_mindfulbreathingapp.ui.ViewModelFactory
import com.mukundan.breathe_mindfulbreathingapp.ui.adapters.SleepChildAdapter
import com.mukundan.breathe_mindfulbreathingapp.ui.base.BaseFragment
import dagger.android.support.AndroidSupportInjection
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class SleepChildFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var sleepChildViewModel: SleepChildViewModel

    private lateinit var binding: SleepChildFragmentBinding

    var sleeps = ArrayList<NewsItem>()

    override val layoutId: Int
        get() = R.layout.sleep_child_fragment

    override fun initViewModel() {
        sleepChildViewModel = viewModelFactory.create(SleepChildViewModel::class.java)
    }

    companion object {

        private const val DATA = "my_data"
        fun newInstance(aBoolean: Boolean) = SleepChildFragment().apply {
            arguments = bundleOf(
                DATA to aBoolean
            )
        }

    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)

    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = SleepChildFragmentBinding.bind(view)
        initViewModel()
        initViews()
    }

    private fun initViews() {

        bindListData()
    }

    private fun bindListData() {

        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())
        sleeps.add(NewsItem())

        val newsAdapter = SleepChildAdapter(sleepChildViewModel, sleeps)

        binding.sleepListRv.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
        binding.sleepListRv.layoutManager = GridLayoutManager(context, 2)
       // binding.sleepListRv.addItemDecoration(GridSpacingItemDecoration(2, 5, true))

        binding.sleepListRv.adapter = ScaleInAnimationAdapter(newsAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }




//        if (!(newsModel.newsItems.isNullOrEmpty())) {
//
////            showDataView(true)
//        } else {
////            showDataView(false)
//        }
//        EspressoIdlingResource.decrement()
    }
}
